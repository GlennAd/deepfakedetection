import os
import cv2
import dlib
import time
import argparse
import numpy as np
from imutils import video
import random
import subprocess

DOWNSAMPLE_RATIO = 4
CROP_FACTOR = 1.3
TRAINING_VALIDATION_RATIO = 0.8

COUNT = 0
SKIP_FRAMES = 1
FRAME_COUNT = 0

def reshape_for_polyline(array):
    return np.array(array, np.int32).reshape((-1, 1, 2))

def cap_to_folder(cap, destination):
    global COUNT
    global FRAME_COUNT

    fps = video.FPS().start()

    while cap.isOpened():
        ret, frame = cap.read()
        FRAME_COUNT += 1
        
        if not ret:
            break
        
        if FRAME_COUNT % SKIP_FRAMES != 0:
            continue

        frame_resize = cv2.resize(frame, None, fx=1 / DOWNSAMPLE_RATIO, fy=1 / DOWNSAMPLE_RATIO)
        gray = cv2.cvtColor(frame_resize, cv2.COLOR_BGR2GRAY)
        faces = detector(gray, 1)
        black_image = np.zeros(frame.shape, np.uint8)

        t = time.time()

        # Perform if there is a face detected
        if len(faces) == 1:
            xMin = 100000
            xMax = 0
            yMin = 100000
            yMax = 0
        
            for face in faces:
                detected_landmarks = predictor(gray, face).parts()
                landmarks = [[p.x * DOWNSAMPLE_RATIO, p.y * DOWNSAMPLE_RATIO] for p in detected_landmarks]

                jaw = reshape_for_polyline(landmarks[0:17])
                left_eyebrow = reshape_for_polyline(landmarks[22:27])
                right_eyebrow = reshape_for_polyline(landmarks[17:22])
                nose_bridge = reshape_for_polyline(landmarks[27:31])
                lower_nose = reshape_for_polyline(landmarks[30:35])
                left_eye = reshape_for_polyline(landmarks[42:48])
                right_eye = reshape_for_polyline(landmarks[36:42])
                outer_lip = reshape_for_polyline(landmarks[48:60])
                inner_lip = reshape_for_polyline(landmarks[60:68])

                color = (255, 255, 255)
                thickness = 3

                cv2.polylines(black_image, [jaw], False, color, thickness)
                cv2.polylines(black_image, [left_eyebrow], False, color, thickness)
                cv2.polylines(black_image, [right_eyebrow], False, color, thickness)
                cv2.polylines(black_image, [nose_bridge], False, color, thickness)
                cv2.polylines(black_image, [lower_nose], True, color, thickness)
                cv2.polylines(black_image, [left_eye], True, color, thickness)
                cv2.polylines(black_image, [right_eye], True, color, thickness)
                cv2.polylines(black_image, [outer_lip], True, color, thickness)
                cv2.polylines(black_image, [inner_lip], True, color, thickness)
                
                for l in landmarks:
                    if l[1] < xMin and l[1] >= 0:
                        xMin = l[1]
                    if l[1] > xMax:
                        xMax = l[1]
                    if l[0] < yMin and l[0] >= 0:
                        yMin = l[0]
                    if l[0] > yMax:
                        yMax = l[0]                
                

            # Display the resulting frame
            COUNT += 1
            #print(COUNT)
            #print(frame)
            
            xDiff = xMax - xMin
            yDiff = yMax - yMin
            
            crop_factor = (CROP_FACTOR - 1) / 2
            xMin -= int(xDiff * crop_factor)
            xMax += int(xDiff * crop_factor)
            yMin -= int(yDiff * crop_factor)
            yMax += int(yDiff * crop_factor)
            
            cv2.imwrite(destination + "{}.png".format(COUNT), frame[xMin:xMax, yMin:yMax])
            #cv2.imwrite("landmarks/{}.png".format(COUNT), black_image[xMin:xMax, yMin:yMax])
            fps.update()

            #print('[INFO] elapsed time: {:.2f}'.format(time.time() - t))

            if COUNT == args.number:  # only take 400 photos
                break
            elif cv2.waitKey(1) & 0xFF == ord('q'):
                break
        else:
            print("No face detected")

    fps.stop()
    #print('[INFO] elapsed time (total): {:.2f}'.format(fps.elapsed()))
    #print('[INFO] approx. FPS: {:.2f}'.format(fps.fps()))

    cap.release()

def cap_video_set(directory, realOrFake):
    files = os.listdir(directory)
    
    
    random.shuffle(files)
    
    l = len(files)
    
    training_files = files[:int(l * TRAINING_VALIDATION_RATIO)]
    testing_files = files[int(l * TRAINING_VALIDATION_RATIO):]
   
    number = 0
    
    for file in training_files:
        print(directory + file)
        print("File ", number, " of ", len(training_files), " training files")
        number += 1
        
        cap = cv2.VideoCapture(directory + file)
        cap_to_folder(cap, "dataset/training/" + realOrFake)
        
    number = 0
    for file in testing_files:
        print(directory + file)
        print("File ", number, " of ", len(testing_files), " testing files")
        number += 1
        
        cap = cv2.VideoCapture(directory + file)
        cap_to_folder(cap, "dataset/testing/" + realOrFake)

def main():
    #os.makedirs('original', exist_ok=True)
    #os.makedirs('landmarks', exist_ok=True)
    os.makedirs('dataset/training', exist_ok=True)
    os.makedirs('dataset/testing', exist_ok=True)
    os.makedirs('dataset/training/fake', exist_ok=True)
    os.makedirs('dataset/training/real', exist_ok=True)
    os.makedirs('dataset/testing/fake', exist_ok=True)
    os.makedirs('dataset/testing/real', exist_ok=True)
    
    #cap_video_set("original_sequences/actors/c23/videos/", "real/")
    #cap_video_set("original_sequences/youtube/c23/videos/", "real/")
    #cap_video_set("manipulated_sequences/DeepFakeDetection/c23/videos/", "fake/")
    #cap_video_set("manipulated_sequences/Deepfakes/c23/videos/", "fake/")
    #cap_video_set("manipulated_sequences/Face2Face/c23/videos/", "fake/")
    #cap_video_set("manipulated_sequences/FaceSwap/c23/videos/", "fake/")
    #cap_video_set("manipulated_sequences/NeuralTextures/c23/videos/", "fake/")
    
    #subprocess.call("py create_tfrecord.py --dataset_dir=./dataset --tfrecord_filename=deepfake")
    
    cv2.destroyAllWindows()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    #parser.add_argument('--file', dest='filename', type=str, help='Name of the video file.')
    parser.add_argument('--num', dest='number', type=int, help='Number of train data to be created.')
    parser.add_argument('--landmark-model', dest='face_landmark_shape_file', type=str, help='Face landmark model file.')
    args = parser.parse_args()

    # Create the face predictor and landmark predictor
    detector = dlib.get_frontal_face_detector()
    predictor = dlib.shape_predictor(args.face_landmark_shape_file)

    main()
